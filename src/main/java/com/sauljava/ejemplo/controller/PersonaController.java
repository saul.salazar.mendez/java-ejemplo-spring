package com.sauljava.ejemplo.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.sauljava.ejemplo.modelo.Persona;
import com.sauljava.ejemplo.repository.PersonaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

@RestController
public class PersonaController {

    @Autowired
    private PersonaRepository personaRepository;

    Logger logger = LogManager.getLogger(PersonaController.class);

    @RequestMapping(value = "/persona", method = RequestMethod.GET)
    public ResponseEntity<List<Persona>> list() {
        Iterable<Persona> personas = personaRepository.findAll();
        List<Persona> list = new ArrayList<Persona>();
        personas.forEach(list::add);
        return new ResponseEntity<List<Persona>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/persona", method = RequestMethod.POST)
    public Persona addPersona(@RequestBody Persona persona){
        Persona  newPersona = personaRepository.save(persona);
        return newPersona;
    }

    @RequestMapping(value = "/persona/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Persona>> getPersona(
        @PathVariable("id") Long id) {
        Optional<Persona> persona = personaRepository.findById(id);        
        return new ResponseEntity <Optional<Persona>>(persona, HttpStatus.OK);
    }

    @RequestMapping(value="/persona/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Persona> update(@PathVariable("id") long id,
                                            @RequestBody Persona persona){
        {
            Optional<Persona> find = personaRepository.findById(id);
            if (!find.isPresent()) {
                Persona nolo = null;
                return new ResponseEntity<Persona>(nolo, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Persona original = find.get();
            original.setApellido(persona.getApellido());
            original.setNombre(persona.getNombre());
            Persona update = personaRepository.save(original);
            return new ResponseEntity<Persona>(update, HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "/persona", method = RequestMethod.DELETE)
    public Boolean deletePersona(@RequestBody JsonNode json){
        if (json.get("id") == null) {
            return false;
        }
        Long id = json.get("id").asLong();
        try {
            personaRepository.deleteById(id);    
        } catch (Exception e) {
            
        }
        
        return true;
    }
}