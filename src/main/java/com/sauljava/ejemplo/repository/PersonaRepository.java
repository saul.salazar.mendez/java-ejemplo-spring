package com.sauljava.ejemplo.repository;

import com.sauljava.ejemplo.modelo.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

}