# Ejemplo de rest api

## Dependecias
Tener instalado
- JDK 1.8
- maven

## Probar
Ejecutar
- build.bat(en unix sh build.bat)
- run.bat(en unix sh run.bat)

## Rutas del api

### Listar personas
Obtiene todas las personas
```
GET    htpp://localhost:12345/persona
```
### Agregar una persona
```
POST   htpp://localhost:12345/persona
```
La petición es json y la siguiente estructura es la ocupada
```
{
    "nombre": "Mario",
    "apellido": "López"
}
```
### Obtener personas
Regresa la persona con el {id} dado
```
GET    http://localhost:12345/persona/{id}
```
### Modificar persona
Modifica la persona con el {id} dado
```
PUT    http://localhost:12345/persona/{id}
```
La petición es json y la siguiente estructura es la ocupada
```
{
    "nombre": "Mario",
    "apellido": "Perez"
}
```
Regresa la persona modificada

### Eliminar persona
Elimina una persona
```
DELETE http://localhost:12345/persona
```
La petición se hace en json
```
{
    "id": 1
}
```
Regresa true o false